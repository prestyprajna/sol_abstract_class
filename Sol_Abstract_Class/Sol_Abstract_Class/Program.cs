﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_Abstract_Class
{
    class Program
    {
        static void Main(string[] args)
        {
            //Test testObj = new Test();  //you cannot create an instance of abstract class

            Test1 testObj = new Test1();
            testObj.Insert();
            testObj.Update();
            testObj.Delete();
            testObj.Display();
        }
    }

    public abstract class Test
    {
        public abstract void Insert();
        public abstract void Update();
        public abstract void Delete();

        public void Display()
        {
            Console.WriteLine("display method");
        }
    }

    public class Test1:Test
    {
        public override void Insert()
        {
            Console.WriteLine("insert method");
        }

        public override void Update()
        {
            Console.WriteLine("update method");
        }

        public override void Delete()
        {
            Console.WriteLine("delete method");
        }
    }
}
